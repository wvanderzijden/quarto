package nl.vanderzijden.quarto;

import com.sun.tools.javac.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Quarto4 {

    public static void main(String[] args) {
        startTime = System.currentTimeMillis();
        long drawsFound = countDraws(getStartBoard(), 0);
        long tt = System.currentTimeMillis() - startTime;
        System.out.println("Time took=" + toTime(tt));
        System.out.println("Positions evaluated=" + evaluatedPositions);
        System.out.println("Draws found=" + drawsFound);
        System.out.println("Ratio=" + (1. * drawsFound / evaluatedPositions));
        System.out.println("Solution " + ((drawsFound == 414298141056L) ? "correct" : "false"));
    }

    static int[] indexShuffle = {0, 5, 10, 15, 14, 13, 12, 9, 1, 6, 3, 2, 7, 11, 4, 8};

    private static final int HASH_MAX_INDEX = 12;

    private static long countDraws(long board, int index) {
        long hash = Long.MAX_VALUE;
        if (index < HASH_MAX_INDEX) {
            //long cc = getSimpleCachedCount3(board, index);
            //if (cc != -1) {
            hash = simpleHash6(board, index);
            long cc = getSimpleCachedCount3(hash, index);
            if (cc != -1) {
                //System.out.println(index);
                evaluatedPositions += factorial[16 - index];
                return cc;
            }
        }
        int indexShuffled = indexShuffle[index];
        long count = 0;
        for (int n = index; n < 16; n++) {
            long newBoard = swap(board, indexShuffled, indexShuffle[n]);
            if (partialEvaluate(newBoard, indexShuffled)) {
                evaluatedPositions += factorial[15 - index];
                continue;
            }
            if (index == 15) {
                evaluatedPositions++;
                count++;
            } else {
                count += countDraws(newBoard, index + 1);
            }
        }
        if (index == 3) {
            double done = 1. * evaluatedPositions / TOTAL_POSITIONS;
            long timeSpent = System.currentTimeMillis() - startTime;
            long timeLeft = (long) (timeSpent / done) - timeSpent;
            System.out.println(evaluatedPositions + "\t" + toTime(timeSpent) + "\t" + toTime(timeLeft)
                    + "\t" + String.format("%.4f", done * 100) + "%");
            //if (timeSpent > 30000)
            //    System.exit(0);
        }
        if (index < HASH_MAX_INDEX) {
            if (hash == Long.MAX_VALUE) {
                System.out.println(toString(board));
                System.out.println(index);
            } else
                cache2.get(index).put(hash, count);
        }
        return count;
    }

    static long simpleHash6(long board, int size) {
        int firstPiece = getPiece(board, indexShuffle[0]);
        Pair<Integer, List<Integer>> pair = firstElementTranspositions.get(firstPiece);
        long signature = pair.fst;
        List<Integer> ts = pair.snd;
        for (int n = 1; n < size; n++) {
            int min = 16;
            List<Integer> ts2 = new ArrayList<>();
            for (int t : ts) {
                int piece = getPiece(board, indexShuffle[n]);
                int posId = transpositions[piece][t];
                if (posId == min) {
                    ts2.add(t);
                } else if (posId < min) {
                    min = posId;
                    ts2.clear();
                    ts2.add(t);
                }
            }
            ts = ts2;
            signature = signature << 4 | min;
        }
        return signature;
    }

    static int getPiece(long board, int position) {
        return (int) (board >>> (position << 2)) & 0xf;
    }

    static long simpleHash5(long board, int size) {
        long signature = Long.MAX_VALUE;
        List<Integer> ts = IntStream.range(0, 48).boxed().collect(Collectors.toList());
        for (int n = 0; n < size; n++) {
            int min = 16;
            List<Integer> ts2 = new ArrayList<>();
            for (int t : ts) {
                int position = indexShuffle[n];
                int piece = (int) ((board >>> (position * 4)) & 0xf);
                int posId = transpositions[piece][t];
                if (posId == min) {
                    ts2.add(t);
                } else if (posId < min) {
                    min = posId;
                    ts2.clear();
                    ts2.add(t);
                }
            }
            ts = ts2;
            signature = signature << 4 | min;
        }
        return signature;
    }

    static long simpleHash4(long board, int size) {
        long min = Long.MAX_VALUE;
        for (int t = 0; t < 48; t++) {
            long signature = 0;
            for (int n = 0; n < size; n++) {
                int position = indexShuffle[n];
                int piece = (int) ((board >>> (position * 4)) & 0xf);
                long posId = transpositions[piece][t];
                signature = signature << 4 | posId;
            }
            if (signature < min)
                min = signature;
        }
        return min;
    }

    static long simpleHash3(long board, int size) {
        long hash = 1;
        Set<Long> signatures = new HashSet<>();
        for (int t = 0; t < 48; t++) {
            long signature = 0;
            for (int n = 0; n < size; n++) {
                int position = indexShuffle[n];
                int piece = (int) ((board >>> (position * 4)) & 0xf);
                long posId = transpositions[piece][t];
                signature = signature << 4 | posId;
            }
            signatures.add(signature);
        }
        for (long signature : signatures)
            hash ^= new Random(signature).nextLong();
        return hash;
    }

    private static long getSimpleCachedCount3(long hash, int index) {
        if (cache2.get(index).containsKey(hash)) {
            return cache2.get(index).get(hash);
        }
        return -1;
    }

    private static long hash(long board, int index) {
        long hash = 0;
        for (int t = 0; t < 48; t++) {
            long h = 0;
            for (int n = 0; n < index; n++) {
                int position = indexShuffle[n];
                int piece = (int) (board >>> (position * 4)) & 0xf;
                h ^= zobristTable[position << 4 | piece][t];
            }
            hash ^= h;
        }
        return hash;
    }

    /*
    private static long hash(long board, int index) {
        long hash = 0;
        for (int t = 0; t < 48; t++) {
            long h = 0;
            for (int n = 0; n < index; n++) {
                int position = indexShuffle[n];
                int piece = (int) (board >>> position) & 0xf;
                h += zobristTable[position * 16 + piece][t];
            }
            hash ^= mix(h);
        }
        return mix(hash);
    }
*/
    private static long mix(long x) {
        long a = x >> 32;
        long b = x >> 60;
        x ^= (a >> b);
        return x * 7993060983890856527L;
    }

    private static Map<Long, Long> cache = new HashMap<>();
    private static List<Map<Long, Long>> cache2 = new ArrayList<>();

    static {
        for (int i = 0; i < 16; i++)
            cache2.add(new HashMap<>());
    }


    private static long simpleHash2(long board, int size, int transposition) {
        long hash = 0;
        for (int n = 0; n < size; n++) {
            int position = indexShuffle[n];
            int piece = (int) ((board >>> (position * 4)) & 0xf);
            long posId = transpositions[piece][transposition];
            hash = hash << 4 | posId;
        }
        return hash;
    }

    private static long getSimpleCachedCount2(long board, int index) {
        for (int t = 0; t < 48; t++) {
            long hash = simpleHash2(board, index, t);
            if (cache2.get(index).containsKey(hash)) {
                return cache2.get(index).get(hash);
            }
        }
        return -1;
    }

    private static long simpleHash(long board, int index, int transposition) {
        long hash = 0;
        for (int n = 0; n < index && n < 7; n++) {
            int position = indexShuffle[n];
            long posId = simpleZobrist[position << 4 | ((int) (board >>> (position * 4)) & 0xf)][transposition];
            hash = hash << 8 | posId;
        }
        return hash;
    }

    private static long getSimpleCachedCount(long board, int index) {
        for (int t = 0; t < 48; t++) {
            long hash = simpleHash(board, index, t);
            if (cache.containsKey(hash)) {
                return cache.get(hash);
            }
        }
        return -1;
    }

    private static long generateHash(long board, int index, int transposition) {
        long hash = 0;
        for (int n = 0; n < index; n++) {
            int position = indexShuffle[n];
            hash ^= mix(zobristTable[position * 16 + ((int) (board >>> (position * 4)) & 0xf)][transposition]);
        }
        return hash;
    }

    private static long getCachedCount(long board, int index) {
        for (int t = 0; t < 48; t++) {
            long hash = generateHash(board, index, t);
            if (cache.containsKey(hash)) {
                return cache.get(hash);
            }
        }
        return -1;
    }

    static boolean partialEvaluate(long board, int index) {
        switch (index) {
            case 15:
                return evaluate(board, masks[8]);
            case 12:
                return evaluate(board, masks[3]);
            case 1:
                return evaluate(board, masks[5]);
            case 3:
                return evaluate(board, masks[9]);
            case 2:
                return evaluate(board, masks[0]) || evaluate(board, masks[6]);
            case 11:
                return evaluate(board, masks[7]);
            case 4:
                return evaluate(board, masks[1]);
            case 8:
                return evaluate(board, masks[4]) || evaluate(board, masks[2]);
        }
        return false;
    }

    private static long evaluatedPositions;
    private static long startTime;

    private static final long TOTAL_POSITIONS = factorial(16);

    private static final long ROW_MASK = 1L | 1L << 4 | 1L << 8 | 1L << 12;
    private static final long COL_MASK = 1L | 1L << 16 | 1L << 32 | 1L << 48;
    private static final long FIRST_DIAG_MASK = 1L | 1L << 20 | 1L << 40 | 1L << 60;
    private static final long SECOND_DIAG_MASK = 1L << 12 | 1L << 24 | 1L << 36 | 1L << 48;

    private static boolean evaluate(long board, long[] masks) {
        return _evaluate(board, masks) || _evaluate(~board, masks);
    }

    private static boolean _evaluate(long board, long[] masks) {
        for (long mask : masks)
            if ((board & mask) == mask)
                return true;
        return false;
    }

    private static long swap(long board, int x, int y) {
        if (x == y)
            return board;
        if (x > y)
            return swap(board, y, x);
        long xValue = (board & swapMasks[1][x]) << ((y - x) * 4);
        long yValue = (board & swapMasks[1][y]) >>> ((y - x) * 4);
        return board & swapMasks[0][x] & swapMasks[0][y] | xValue | yValue;
    }

    private static long getStartBoard() {
        long board = 0;
        for (long n = 0; n < 16; n++)
            board |= n << (n * 4);
        return board;
    }

    private static int[][] transpositions = new int[16][48];

    static {

        for (int piece = 0; piece < 16; piece++) {
            transpositions[piece][0] = piece;
            List<Integer> permutations = allPermutations(piece, 4, 0, new ArrayList<>());
            for (int n = 1; n < 24; n++) {
                transpositions[piece][n] = permutations.get(n);
            }
            permutations = allPermutations(~piece & 0xf, 4, 0, new ArrayList<>());
            for (int n = 24; n < 48; n++) {
                transpositions[piece][n] = permutations.get(n - 24);
            }
        }
        /*
        for (int[] row : transpositions) {
            for (int cell : row) {
                System.out.print(String.format("%4s", Integer.toBinaryString(cell)).replace(' ', '0') + "\t");
            }
            System.out.println();
        }*/
    }

    static List<Pair<Integer,List<Integer>>> firstElementTranspositions = new ArrayList<>();

    static {
        for (int n = 0; n < 16; n++) {
            int min = 16;
            List<Integer> elems = new ArrayList<>();
            for (int t = 0; t < 48; t++) {
                int elem = transpositions[n][t];
                if (elem < min) {
                    min = elem;
                    elems.clear();
                    elems.add(t);
                } else if (elem == min) {
                    elems.add(t);
                }
            }
            firstElementTranspositions.add(new Pair<>(min, elems));
        }
    }

    private static long[][] simpleZobrist = new long[16 * 16][48];

    static {
        for (int pos = 0; pos < 16; pos++) {
            for (int piece = 0; piece < 16; piece++) {
                simpleZobrist[pos << 4 | piece][0] = pos << 4 | piece;
            }
        }
        for (int pos = 0; pos < 16; pos++) {
            for (int piece = 0; piece < 16; piece++) {
                List<Integer> permutations = allPermutations(piece, 4, 0, new ArrayList<>());
                for (int n = 1; n < 24; n++) {
                    simpleZobrist[pos << 4 | piece][n] = simpleZobrist[pos << 4 | permutations.get(n)][0];
                }
                permutations = allPermutations(~piece & 0xf, 4, 0, new ArrayList<>());
                for (int n = 24; n < 48; n++) {
                    simpleZobrist[pos << 4 | piece][n] = simpleZobrist[pos << 4 | permutations.get(n - 24)][0];
                }
            }
        }
    }

    private static long[][] zobristTable = new long[16 * 16][48];
    private static Random rand = new Random();

    static {
        for (int pos = 0; pos < 16; pos++) {
            for (int piece = 0; piece < 16; piece++) {
                zobristTable[pos << 4 | piece][0] = rand.nextLong();
            }
        }
        for (int pos = 0; pos < 16; pos++) {
            for (int piece = 0; piece < 16; piece++) {
                List<Integer> permutations = allPermutations(piece, 4, 0, new ArrayList<>());
                for (int n = 1; n < 24; n++) {
                    zobristTable[pos << 4 | piece][n] = zobristTable[pos << 4 | permutations.get(n)][0];
                }
                permutations = allPermutations(~piece & 0xf, 4, 0, new ArrayList<>());
                for (int n = 24; n < 48; n++) {
                    zobristTable[pos << 4 | piece][n] = zobristTable[pos << 4 | permutations.get(n - 24)][0];
                }
            }
        }
    }

    private static List<Integer> allPermutations(int input, int size, int idx, List<Integer> permutations) {
        for (int n = idx; n < size; n++) {
            if (idx == 3) {
                permutations.add(input);
            }
            allPermutations(swapBit(input, idx, n), size, idx + 1, permutations);
        }
        return permutations;
    }

    private static int swapBit(int in, int x, int y) {
        if (x == y)
            return in;
        int xMask = 1 << x;
        int yMask = 1 << y;
        int xValue = (in & xMask) << (y - x);
        int yValue = (in & yMask) >>> (y - x);
        return in & ~xMask & ~yMask | xValue | yValue;
    }

    private static long[][] masks = new long[10][4];

    static {
        for (int m = 0; m < 4; m++) {
            long row = ROW_MASK << (16 * m);
            for (int n = 0; n < 4; n++)
                masks[m][n] = row << n;
        }
        for (int m = 0; m < 4; m++) {
            long row = COL_MASK << (4 * m);
            for (int n = 0; n < 4; n++)
                masks[m + 4][n] = row << n;
        }
        for (int n = 0; n < 4; n++) {
            masks[8][n] = FIRST_DIAG_MASK << n;
        }
        for (int n = 0; n < 4; n++) {
            masks[9][n] = SECOND_DIAG_MASK << n;
        }
    }

    private static long[][] swapMasks;

    static {
        swapMasks = new long[2][16];
        for (int n = 0; n < 16; n++)
            swapMasks[1][n] = 0xfL << (n * 4);
        for (int n = 0; n < 16; n++)
            swapMasks[0][n] = ~swapMasks[1][n];
    }

    private static long[] factorial = new long[17];

    static {
        for (int n = 0; n < factorial.length; n++)
            factorial[n] = factorial(n);
    }

    private static long factorial(long n) {
        long result = 1;
        while (n > 1)
            result *= n--;
        return result;
    }

    private static String toTime(long ts) {
        int ms = (int) (ts % 1000);
        int seconds = (int) (ts / 1000) % 60;
        int minutes = (int) ((ts / (1000 * 60)) % 60);
        int hours = (int) ((ts / (1000 * 60 * 60)));
        if (hours > 0)
            return String.format("%d:%02d:%02d.%03d", hours, minutes, seconds, ms);
        if (minutes > 0)
            return String.format("%02d:%02d.%03d", minutes, seconds, ms);
        return String.format("%02d.%03d", seconds, ms);
    }

    public static String toString(long board) {
        String s = String.format("%64s", Long.toBinaryString(board)).replace(' ', '0');
        StringBuilder sb = new StringBuilder();
        for (int m = 0; m < 4; m++) {
            for (int n = 0; n < 4; n++)
                sb.append(s.substring(m * 16 + n * 4, m * 16 + n * 4 + 4)).append(" ");
            sb.append("\n");
        }
        return sb.toString();
    }

}
