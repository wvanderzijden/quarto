package nl.vanderzijden.quarto;

public class Quarto {

	public static void main(String[] args)
	{
		for (long n = Long.MIN_VALUE; n < Long.MAX_VALUE; n++) {
			Board board = new Board(n);
			if (board.evaluate())
				System.out.println(board);
		}		
	}
	
	public static class Board
	{
		public final long board;
		
		public Board(long board)
		{
			this.board = board;
		}
		
		public int getAt(int r, int c)
		{
			return (int) (board >> (r * 16 + c * 4)) & 0xf;
		}
		
		@Override
		public String toString()
		{
			StringBuilder sb = new StringBuilder(board + "\n");
			for (int r = 0; r < 4; r++)
			{
				for (int c = 0; c < 4; c++)
					sb.append(Long.toBinaryString(getAt(r,c)) + "\t");
				sb.append("\n");
			}
			return sb.toString();
		}
		
		public boolean evaluate()
		{
			for (int r = 0; r < 4; r++)
				if (evaluate(getAt(r,0), getAt(r,1), getAt(r,2), getAt(r,3)))
					return true;
			for (int c = 0; c < 4; c++)
				if (evaluate(getAt(0,c), getAt(1,c), getAt(2,c), getAt(3,c)))
					return true;
			return false;
		}
		
		private boolean evaluate(int...vals)
		{
			return _evaluate(vals) || _evaluate(invertAll(vals));
		}
		
		private int[] invertAll(int...vals)
		{
			for (int n = 0; n < 4; n++)
				vals[n] = ~vals[n];
			return vals;
		}
		
		private boolean _evaluate(int... vals)
		{
			int mask = 0xf;
			for (int v : vals)
				mask &= v;
			return mask > 0;
			
		}
	}
}
