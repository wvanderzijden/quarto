package nl.vanderzijden.quarto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestQuarto {

    @Test
    public void testHashing()
    {
        long board = 8 << 20 | 15;
        System.out.println(Quarto4.toString(board));
        System.out.println(Quarto4.simpleHash3(board, 2));
        long board2 = 1 << 20 | 15;
        System.out.println(Quarto4.toString(board2));
        assertEquals(Quarto4.simpleHash3(board, 2), Quarto4.simpleHash3(board2, 2));
    }
}
