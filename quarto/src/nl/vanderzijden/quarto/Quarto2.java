package nl.vanderzijden.quarto;

public class Quarto2 {

	public static void main(String[] args) {
		long before = System.currentTimeMillis();
		BoardBuilder bb = new BoardBuilder(4);
		bb.findDraw(bb.board, 0);
		long tt = System.currentTimeMillis() - before;
		System.out.println("Time took=" + (tt / 1000) + "." + String.format("%03d", (tt % 1000)));
		System.out.println("Positions evaluated=" + bb.count);
		System.out.println("Draws found=" + bb.countDraws);
	}

	public static void swap(int[] board, int i, int j) {
		int tmp = board[i];
		board[i] = board[j];
		board[j] = tmp;
	}

	public static class BoardBuilder {
		private final int[] board;
		private final int N;
		private final int size;
		private final int pieces;
		private final Boolean[] cache;

		public BoardBuilder(int N) {
			this.N = N;
			pieces = (int) Math.pow(2, N);
			size = (int) Math.sqrt(pieces);
			board = new int[size * size];
			cache = new Boolean[1 << (N * size)];
			initBoard();
		}

		public int[] initBoard() {
			int value = 0;
			for (int n = 0; n < board.length; n++) {
				board[n] = value;
				value = (value + 1) % pieces;
			}
			return board;
		}

		private long count;
		private long countDraws;

		private void findDraw(int[] board, int idx) {
			if (idx == board.length) {
				count++;
				if (count % 100000000 == 0)
					System.out.println(count);
				if (!evaluateBoard(board))
				{
					//System.out.println(toString(board));
					countDraws++;
				}
				return;
			}
			if (idx > 0 && idx % size == 0 && evaluateRow(board, (idx / size) - 1))
				return;
			for (int i = idx; i < board.length; i++) {
				swap(board, i, idx);
				findDraw(board, idx + 1);
				swap(board, idx, i);
			}
		}

		public boolean evaluateBoard(int[] board) {
			for (int r = 0; r < size; r++)
				if (evaluateRow(board, r))
					return true;
			for (int c = 0; c < size; c++)
				if (evaluateCol(board, c))
					return true;
			return evaluateDiags(board);
		}

		public boolean evaluateCol(int[] board, int c) {
			int[] values = new int[size];
			for (int r = 0; r < size; r++)
				values[r] = board[r * size + c];
			return evaluate(values);
		}

		public boolean evaluateRow(int[] board, int r) {
			int[] values = new int[size];
			for (int c = 0; c < size; c++)
				values[c] = board[r * size + c];
			return evaluate(values);
		}

		private boolean evaluateDiags(int[] board) {
			int[] values = new int[size];
			for (int cr = 0; cr < size; cr++)
				values[cr] = board[cr * size + cr];
			if (evaluate(values))
				return true;
			int r = size - 1;
			for (int c = 0; c < size; c++)
				values[c] = board[r-- * size + c];
			return evaluate(values);
		}

		private boolean evaluate(int... values) {
			int key = valuesToInt(values);
			Boolean result = cache[key];
			if (result == null) {
				result = _evaluate(values) || _evaluate(invertAll(values));
				cache[key] = result;
			}
			return result;
		}
		
		private int valuesToInt(int...values)
		{
			int result = 0;
			for (int v : values)
				result = (result << N) | v;
			return result;
		}
		
		private boolean _evaluate(int... values) {
			int mask = pieces - 1;
			for (int val : values)
				mask &= val;
			return mask > 0;
		}

		private int[] invertAll(int... vals) {
			for (int n = 0; n < size; n++)
				vals[n] = ~vals[n];
			return vals;
		}

		public String toString(int[] board) {
			StringBuilder sb = new StringBuilder();
			for (int n = 0; n < size; n++) {
				for (int m = 0; m < size; m++)
					sb.append(String.format("%" + size + "s", Integer.toBinaryString(board[n * size + m])).replace(' ',
							'0') + "\t");
				sb.append("\n");
			}
			return sb.toString();
		}
	}
}
