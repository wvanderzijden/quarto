package nl.vanderzijden.quarto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MarkII {

    public static void main(String[] args) throws FileNotFoundException {
        try (Scanner in = new Scanner(new File("quarto/src/nl/vanderzijden/quarto/MarkII.java"))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                for (char c : line.toCharArray()) {
                    System.out.print("\\u" + String.format("%4s", Integer.toHexString((int) c)).replace(' ', '0'));
                }
                System.out.println();
            }
        }
    }
}
