package nl.vanderzijden.quarto;

import java.util.HashMap;
import java.util.Map;

public class SB {

    public static void main(String[] args) {
        System.out.println(countPermutations("cbadcabc", "abc"));
    }

    static int countPermutations(String text, String symbols) {
        Map<Character, Integer> original = createMap(symbols);
        Map<Character, Integer> copy = new HashMap<>(original);;
        int count = 0;
        int seq = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (copy.containsKey(c) && copy.get(c) > 0) {
                copy.put(c, copy.get(c) - 1);
                seq++;
            }
            else if (seq > 0) {
                seq = 0;
                copy = new HashMap<>(original);
            }
            if (seq == symbols.length()) {
                count++;
                seq--;
                copy.put(text.charAt(i - seq), copy.get(text.charAt(i - seq)) + 1);
            }
        }
        return count;
    }

    static Map<Character, Integer> createMap(String symbols) {
        Map<Character, Integer> ss = new HashMap<>();
        for (int i = 0; i < symbols.length(); i++) {
            char c = symbols.charAt(i);
            if (!ss.containsKey(c))
            ss.put(c, 1);
		else
            ss.put(c, ss.get(c) + 1);
        }
        return ss;
    }

}
