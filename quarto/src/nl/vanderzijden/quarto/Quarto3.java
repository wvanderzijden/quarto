package nl.vanderzijden.quarto;

import java.util.ArrayList;
import java.util.List;

public class Quarto3 {

	public static void main(String[] args) {
		startTime = System.currentTimeMillis();
		recurse(getStartBoard(), 0);
		long tt = System.currentTimeMillis() - startTime;
		System.out.println("Time took=" + toTime(tt));
		System.out.println("Positions evaluated=" + evaluatedPositions);
		System.out.println("Draws found=" + drawsFound);
	}

	private static void recurse(long board, int index) {
		if (index == 15) {
			evaluatedPositions++;
			if (evaluatedPositions % 100000000L == 0) {
				double done = 1. * evaluatedPositions / TOTAL_POSITIONS;
				long timeSpent = System.currentTimeMillis() - startTime;
				long timeLeft = (long) (timeSpent / done) - timeSpent;
				System.out.println(evaluatedPositions + "\t" + drawsFound + "\t" + toTime(timeSpent) + "\t" + toTime(timeLeft) + "\t" + String.format("%.4f", done * 100) + "%");
			}
			if (!evaluate(board)) {
				drawsFound++;
			}
		} else if (index > 0 && index % 4 == 0 && evaluateRow(board, index / 4 - 1)) {
			evaluatedPositions += ROWSKIPS[index / 4 - 1]; 
		} else {
			for (int n = index; n < 16; n++)
				recurse(swap(board, index, n), index + 1);
		}
	}

	private static long evaluatedPositions;
	private static long drawsFound;
	private static long startTime;

	private static final long[] ROWSKIPS = {faculty(12), faculty(8), faculty(4)};
	private static final long TOTAL_POSITIONS = faculty(16);

	private static final long ROW_MASK = 1L | 1L << 4 | 1L << 8 | 1L << 12;
	private static final long COL_MASK = 1L | 1L << 16 | 1L << 32 | 1L << 48;
	private static final long FIRST_DIAG_MASK = 1L | 1L << 20 | 1L << 40 | 1L << 60;
	private static final long SECOND_DIAG_MASK = 1L << 12 | 1L << 24 | 1L << 36 | 1L << 48;

	private static boolean evaluate(long board) {
		return _evaluate(board) || _evaluate(~board);
	}

	private static boolean _evaluate(long board) {
		for (long mask : masks)
			if ((board & mask) == mask)
				return true;
		return false;
	}

	private static boolean evaluateRow(long board, int r) {
		return _evaluateRow(board, r) || _evaluateRow(~board, r);
	}
	
	private static boolean _evaluateRow(long board, int r) {
		for (int n = r * 4; n < r * 4 + 4; n++)
			if ((board & masks[n]) == masks[n])
				return true;
		return false;
	}

	private static long swap(long board, int x, int y) {
		assert x <= y;
		if (x == y)
			return board;
		long xValue = (board & swapMasks[1][x]) << ((y - x) * 4);
		long yValue = (board & swapMasks[1][y]) >>> ((y - x) * 4);
		return board & swapMasks[0][x] & swapMasks[0][y] | xValue | yValue;
	}

	private static long getStartBoard() {
		long board = 0;
		for (long n = 0; n < 16; n++)
			board |= n << (n * 4);
		return board;
	}

	private static long[] masks;
	static {
		List<Long> tMasks = new ArrayList<>();
		for (int m = 0; m < 4; m++) {
			long row = ROW_MASK << (16 * m);
			for (int n = 0; n < 4; n++)
				tMasks.add(row << n);
		}
		for (int m = 0; m < 4; m++) {
			long row = COL_MASK << (4 * m);
			for (int n = 0; n < 4; n++)
				tMasks.add(row << n);
		}
		for (int m = 0; m < 4; m++) {
			tMasks.add(FIRST_DIAG_MASK << m);
		}
		for (int m = 0; m < 4; m++) {
			tMasks.add(SECOND_DIAG_MASK << m);
		}
		masks = tMasks.stream().mapToLong(x -> x).toArray();
	}

	private static long[][] swapMasks;
	static {
		swapMasks = new long[2][16];
		for (int n = 0; n < 16; n++)
			swapMasks[1][n] = 0xfL << (n * 4);
		for (int n = 0; n < 16; n++)
			swapMasks[0][n] = ~swapMasks[1][n];
	}
	
	private static long faculty(long n)
	{
		long result = 1;
		while (n > 1)
			result *= n--;
		return result;
	}
	
	private static String toTime(long ts)
	{
		int ms = (int) (ts % 1000);
		int seconds = (int) (ts / 1000) % 60 ;
		int minutes = (int) ((ts / (1000*60)) % 60);
		int hours   = (int) ((ts / (1000*60*60)));
		if (hours > 0)
			return String.format("%d:%02d:%02d.%03d", hours, minutes, seconds, ms);
		if (minutes > 0)
			return String.format("%02d:%02d.%03d", minutes, seconds, ms);
		return String.format("%02d.%03d", seconds, ms);
	}

	private static String toString(long board) {
		String s = String.format("%64s", Long.toBinaryString(board)).replace(' ', '0');
		StringBuilder sb = new StringBuilder();
		for (int m = 0; m < 4; m++) {
			for (int n = 0; n < 4; n++)
				sb.append(s.substring(m * 16 + n * 4, m * 16 + n * 4 + 4)).append(" ");
			sb.append("\n");
		}
		return sb.toString();
	}

}
